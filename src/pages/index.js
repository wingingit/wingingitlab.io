import React from "react"
// import { Link } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"

const IndexPage = () => (
  <Layout>
    <SEO title="Home" keywords={[`wingit`, `winging it`, `wing it`, `hackathon`]} />
    <h1>Hello, we are WingingIT.</h1>
    <p>Flying by the seat of our pants into the cold night.</p>
    <p>Now go build something great.</p>
    {/* <Link to="/page-2/">Go to page 2</Link> */}
  </Layout>
)

export default IndexPage
