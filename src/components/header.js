import { Link } from "gatsby"
import PropTypes from "prop-types"
import React from "react"

import logo from "../images/logo.svg"

const Header = ({ siteTitle }) => (
  <header
    style={{
      //background: `rebeccapurple`,
      color: '#2d2822',
      marginBottom: `1.45rem`,
      textAlign: 'center',
    }}
  >
    <div
      style={{
        margin: `0 auto`,
        maxWidth: 960,
        padding: `1.45rem 1.0875rem`,
      }}
    >
      <h1 style={{ margin: 0 }}>
        <Link
          to="/"
          style={{
            color: '#2d2822',
            textDecoration: `none`,
            fontFamily: "'Lakki Reddy', cursive",
            fontSize: '48px',
          }}
        >
          {siteTitle}
        </Link>
      </h1>
      <img style={{width: '200px', marginLeft: '-12px', marginTop: '-23px'}}
        src={logo}/>
    </div>
  </header>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
